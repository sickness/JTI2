local Show = newScene()
Show.queue = {}

function Show:init()
  self.bbcode = require("script.bbcode")
  self.textfont = love.graphics.newFont("gui/font/Hobo.ttf", 15*fontsize)
  self.titlefont = love.graphics.newFont("gui/font/Hobo.ttf", 30*fontsize)
  self.dicefont = love.graphics.newFont("gui/font/Hobo.ttf", 20)
  self.text = "This is a text"
  self.titletext = "Title"
  
  self.editmode = false
  if self.editormode then
    self.editor = require("script.editor")
    self.editor.scene = self
    self.editor:init()
  end
  
  Signal.emit("load-quest")
  
  self.goNext = function(object, x, y)
    if Effect.before[1] ~= nil then
      Effect.Timer:clear()
      self.imagebutton.OnClick = nil
      Effect.runBeforeQueue()
    else
      self.index = self.index + 1
      if self.queue[self.index] == nil then
        Gamestate.switch(scene.list.map)
        Signal.emit("exit-location", player.loc)
      else
        local id = self.queue[self.index]
        if self.editmode == true and self.itext:GetEditable() then
          self.index = self.index - 1
          self.editor:SetEditable(false)
          self.index = self.index + 1
        end
        
        self:loadSlide()
        Signal.emit("nextscene-location", id)
        Effect.runAfterQueue()
        
      end
    end
  end
end

function Show:enter(from)
  self.index = 1
  Timer.clear()
  if Effect then Effect.Timer:clear() end
  if not Effect then Effect = require("script.effect") end
  
  Signal.emit("enter-location", player.loc)
  
  if next(self.queue) == nil then
    Gamestate.switch(scene.list.map)
    return
  end
  
  -- Order metters
  local title = loveframes.Create("textinput", frame)
  title:SetPos(50, 60)
  title:SetWidth(500)
  title:SetHeight(50)
  title:SetMultiline(false)
  title:ShowLineNumbers(false)
  --title:SetFont(self.titlefont)
  --title:SetText(self.titletext)
  title:SetEditable(false)
  --title:SetWrap(false)
  --title.outline = true
  --title.drawtoo = true
  self.ititle = title
  
  self.imagebutton = loveframes.Create("imagebutton", frame)
  self.imagebutton:SetImage("gui/sprite/next_arrow.png")
  --self.imagebutton:SetImageHover("gui/sprite/next_arrow_light.png")
  self.imagebutton:SetPos(900, 10)
  self.imagebutton:SetText("")
  
  self.menubutton = loveframes.Create("imagebutton", frame)
  self.menubutton:SetImage("gui/sprite/next_menu.png")
  --self.menubutton:SetImageHover("gui/sprite/next_menu_light.png")
  self.menubutton:SetPos(840, 10)
  self.menubutton:SetText("")
  self.menubutton.OnClick = function()
    if control.stats_frame then
      control.stats_frame:Remove()
      control.stats_frame = nil
    else control.openStats(player) end
  end
  
  if self.editormode then self.editor:enter() end
    
  self.imagebutton.OnClick = self.goNext
  
  local textinput = loveframes.Create("textinput", frame)
  textinput:SetPos(5, 30)
  textinput:SetWidth(490)
  textinput:SetHeight(200)
  textinput:SetMultiline(true)
  textinput:ShowLineNumbers(false)
  textinput:SetFont(self.textfont)
  textinput:SetText(self.text)
  textinput:SetEditable(false)
  textinput:SetWrap(true)
  --textinput.outline = true
  textinput.drawtoo = true
  self.itext = textinput
  
  --self.itext:SetMouseWheelScrollAmount(1)
  
  self:loadSlide()
  Signal.emit("nextscene-location", self.queue[1])
  Effect.runAfterQueue()
  if from == scene.list.mainmenu and not skip_mainmenu then
    scene.color = {0, 0, 0, 255}
    Timer.tween(5, scene.color, {0, 0, 0, 0}, "linear")
  end
end

function Show:leave()
  self.queue = {}
  loveframes.util.RemoveAll()
end

function Show:loadSlide()
  if battle.running then battle.finish() end
  if self.queue[self.index] ~= nil then
    local name = self.queue[self.index]
    local data = quest.data[name]
    if data == nil then
      data = {background="",
        text={text="http://izildia.blogspot.com", position={350,300}, size={490,200}},
        title={text="Work In Progress", position={350,250}},
      }
      quest.data[name] = data
    end
    
    if data.background ~= "" and love.filesystem.isFile("image/" .. data.background) then
      self.background.image = love.graphics.newImage("image/" .. data.background)  
    else
      local imageData = love.image.newImageData(1920,1080)
      self.background.image = love.graphics.newImage(imageData)
    end
    
    if data.aux and data.aux.path ~= "" and love.filesystem.isFile("image/" .. data.aux.path) then
      if not self.aux then self.aux = {} end
      self.aux.image = love.graphics.newImage("image/" .. data.aux.path)
      self.aux.x = data.aux.x
      self.aux.y = data.aux.y
    else
      if self.aux then self.aux.image = nil end
    end
    
    self.itext:SetText(data.text.text)
    self.itext:SetPos(unpack(data.text.position))
    self.itext:SetSize(unpack(data.text.size))
    self.itext.colormask = {}
    self.ititle:SetText(data.title.text)
    self.ititle:SetPos(unpack(data.title.position))
    
    self.bbcode.make(self.itext)
    
    -- Special slides
    if name:starts("battle") then
      battle.start()
    end
    
    -- Cleanup of the old slide
    Effect.cleanup()
    
    for i, call in ipairs(self.bbcode.aftercall) do call() end
    self.bbcode.aftercall = {}
  end
end

local wheelmoved_auto = love.timer.getTime()
function Show:update(dt)
  if self.editormode then self.editor:update(dt) end
  
  -- Workarround for autoscroll bug.
  if self.itext:GetAutoScroll() and self.itext.vbar and love.timer.getTime() - wheelmoved_auto > 1 then
    local vbar = self.itext:GetVerticalScrollBody().internals[1].internals[1]
    vbar:Scroll(5)
  end
end

function Show:wheelmoved(x, y)
  wheelmoved_auto = love.timer.getTime()
  if not self.itext.vbar then return end
  local vbar = self.itext:GetVerticalScrollBody().internals[1].internals[1]
  vbar:Scroll(-y*3)
end

function Show:keyreleased(key)
  self.editor:keyreleased(key)
end

function Show:draw()
  scene.camera:attach()
  love.graphics.draw(self.background.image, 0, 0, 0, 0.5, 0.5)
  if self.aux and self.aux.image and self.aux.image ~= "" then
    love.graphics.draw(self.aux.image, self.aux.x, self.aux.y, 0, 0.5, 0.5)
  end

  
  loveframes.draw()
  
  -- Draw other elements
  local dicetext = player.dice:getString()
  love.graphics.setColor(255,255,255,255)
  love.graphics.setFont(self.dicefont)
  love.graphics.print(dicetext, 960-self.dicefont:getWidth(dicetext) - 10, 540-40)
  book.draw()
  
  -- Draw the balck fade
  love.graphics.setColor(unpack(scene.color))
  love.graphics.draw(byte_image, 0, 0, 0, 960, 540)
  love.graphics.setColor(255, 255, 255, 255)
  
  -- Some effect may draw over the black fade.
  Effect.draw()
  
  scene.camera:detach()
  
  -- Debug
  --[[
  for i, m in pairs(self.itext.colormask) do
    debug_text = debug_text .. m[1] .. " " .. m[2] .. " " .. m[3] .. " " .. m[4] .. "\n"
  end
  love.graphics.setFont(love_default_font)
  love.graphics.print(debug_text)
  debug_text = ""
  --]]
end

return Show