--- The Player class (Singleton, Global) {NPC}
--
-- This is the object that represents you and your place in the world; It also contains the combat mechanics for the player as well as references to it's stats, inventory, skills, etc...
-- @classmod Player

local map_scene = scene.list.map

local Player = {
  image = love.graphics.newImage("gui/sprite/player.png"),
  loc = "assembly_hall",
  map = "elf_forest",
  dice = require("script.dice"),
  weapon = battle.items.create_weapon("fists", "Your bare fists", 0, 200),
  armature = {battle.items["elf clothes"]},
  stat = battle.create_stat_table(),
  name = "you"
}

Player.hp = {100, 100, 1}
Player.mp = {0, 0, 0}
Player.sp = {50, 50, 4}

function Player:init()
  self.posx = map_scene.location[player.loc]:GetX()+30
  self.posy = map_scene.location[player.loc]:GetY()+30

  self:activateNearLocations()
end

--- Makes all near locations selectable on the map scene.
-- @function activateNearLocations
-- @fixme It should be part of the map scene instead.
function Player:activateNearLocations()
  local near = roads[player.loc]
  for _, loc in pairs(near:split('|')) do
    map_scene.location[loc].visible = true
  end
end

--- Makes all locations unselectable on the map scene.
-- @function deactivateNearLocations
-- @fixme It should be part of the map scene instead.
function Player:deactivateAllLocations()
  for k, loc in pairs(map_scene.location) do
    loc.visible = false
  end
end

--- Moves the player to a specific location
-- @function moveTo
-- @param loc A location from the scene.map.roads (roads is global)
-- @param t The time that will take the movment, if nil it's automatically calculted using the distance and a speed of 0.015
function Player:moveTo(loc, t)
  -- print("Move to: ", loc)
  if t == nil then
    local _x, _y = map_scene.location[self.loc]:GetPos()
    local _j, _k = map_scene.location[loc]:GetPos()
    t = math.sqrt((_j-_x)^2+(_k-_y)^2) * 0.015
  end
  
  self:deactivateAllLocations()
  if self.timer then Timer.cancel(self.timer) end
  local location = map_scene.location[loc]
  self.timer = Timer.tween(t, self, {posx = location:GetX()+30, posy = location:GetY()+30}, "linear", 
    function() 
      self.loc = loc
      Gamestate.switch(scene.list.slideshow)
    end
  ) 
end

--- The attack method for combat.
-- @param npc Who to attack (reference), it must be spawned first.
function Player:attack(npc)
  local sval = battle.calc_stacons(self)
  if player.sp[1] >= sval then
    self.sp[1] = self.sp[1] - sval
    if player.hp[1] < 20 and math.random(1,10) > 8 then
      battle.log("Critical! You ain't gona fall so easly, you gather the last remains of your strength and attack " .. npc.name .. " with a super strong hit!")
      npc:recieveAttack(self.stat.attack.f*2)
    else
      battle.log("You attack " .. npc.name .. " with a strong hit!")
      npc:recieveAttack(self.stat.attack.f)
    end
  else
    battle.log("You try to attack, but you are too tired, your lame attack fails blindly into the air.")
  end
end

--- The reciveAttack method for combat.
-- @param damage The raw amount of damage that a NPC applies on you.
function Player:recieveAttack(damage)
  local eva = self.stat.evasion.f
  local def = self.stat.defense.f
  
  if self.hp[1] <= 20 and math.random(1,3) == 1 then
    damage = damage / 3
    battle.log("You stick to life with all your might and defent yourself on the last moment.")
  end
  
  if damage > eva/math.random(1,4) and math.random(0,100) > eva then
    self.hp[1] = self.hp[1] - math.mkp(damage-def) - 1
  else
    battle.log("You've dodged the attack!")
  end
end

return Player