local control = {} 

-- Generic stuff like:
local function change_frame_background(frame, background)
  local bodyimage = love.graphics.newImage(background)
  frame.Draw = function(object)
    local x = object:GetX()
    local y = object:GetY()
    local width = object:GetWidth()
    local height = object:GetHeight()
    local hover = object:GetHover()
    local name = object:GetName()
    local bodyimage_width = bodyimage:getWidth()
    local bodyimage_height = bodyimage:getHeight()
    local bodyimage_scalex = width/bodyimage_width
    local bodyimage_scaley = height/bodyimage_height
    
    love.graphics.setColor(255,255,255,255)
    love.graphics.draw(bodyimage, x, y, 0, bodyimage_scalex, bodyimage_scaley)
  end
end

function control.MsgBox(_text, title, icon, callback)
  if icon == nil then icon = "" end
  if title == nil then title = "Tutorial" end
  
  local titlefont = love.graphics.newFont("gui/font/Hobo.ttf", 30)
  local textfont = love.graphics.newFont("gui/font/Komika.ttf", 14)
  local _, _t = textfont:getWrap(_text, 350)
  local text = ""
  for _, t in ipairs(_t) do
    text = text .. t .. "\n"
  end
  
  local comming_state = loveframes.GetState()
  loveframes.SetState("MsgBox")
  local frame = loveframes.Create("frame")
  frame:SetSize(400, 120 + #_t * textfont:getHeight())
  frame:SetState("MsgBox")
  frame:SetPos(20, 500-frame:GetHeight())
  frame:CenterX()
  frame:ShowCloseButton(false)
  
  local ititle = loveframes.Create("text", frame)
  ititle:SetText({ {color={100,0,0,255}}, title})
  ititle:SetFont(titlefont)
  ititle:SetPos(0,20)
  ititle:CenterX()
  
  local imessage = loveframes.Create("text", frame)
  imessage:SetText(text)
  imessage:SetFont(textfont)
  imessage:SetPos(0,60)
  imessage:CenterX()
  
  local ibutton = loveframes.Create("button", frame)
  ibutton:SetText("OK")
  ibutton:SetPos(0, frame:GetHeight() - 40)
  ibutton:CenterX()
  ibutton.OnClick = function(object, x, y)
    if frame.OnClose() ~= false then
      frame:Remove()
    end
  end
  
  frame.OnClose = function(object)
    loveframes.SetState(comming_state)
    if callback then callback() end
  end
  
  change_frame_background(frame, "gui/msgbox.png")
  return frame
end

-- Thre shall be the great menu class here.
local function create_button(text, pos)
  local t = loveframes.Create("imagebutton")
  t:SetPos(pos[1] + 800, 490 - pos[2])
  t:SetState(loveframes.GetState())
  t:SetText(text)
  t:SetImage("gui/buttonStock.png")
  t:SetImageHover("gui/buttonStock2.png")
  t:SetSize(150, 50)
  return t
end

function control.create_menu(...)
  local m = { b = {}}
  local arg = {...}
  for i, text in ipairs(arg) do
    m.b[i] = create_button(text, {0, 40*i})
  end
  
  m.AddButton = function(object, i, text)
    if m.b[i] ~= nil then error() end
    m.b[i] = create_button(text, {0, 40*i})
  end
  
  m.Close = function(object) object:SetState("closed") end
  m.Open = function(object)
    local state = loveframes.GetState()
    object:SetState(state)
  end
  
  m.SetState = function(object, state)
    for i, b in ipairs(object.b) do
      b:SetState(state)
    end
  end
  
  m.Remove = function(object)
    for i, b in ipairs(object.b) do
      b:Remove()
    end
    object = nil
  end
  
  return m
end

control.stats_frame = nil
function control.openStats(obj)
  if not control.stats_frame then
    control.stats_frame = loveframes.Create("frame"):SetState(loveframes.GetState())
  else return end
  
  local frame = control.stats_frame
  local titlefont = love.graphics.newFont("gui/font/Gotham.ttf", 20)
  local textfont = love.graphics.newFont("gui/font/Artifika.ttf", 14)
 
  frame:SetSize(318,464)
  frame:Center()
  frame:ShowCloseButton(false)
  change_frame_background(frame, "gui/generic.png")
  
  local ititle = loveframes.Create("text", frame)
  ititle:SetText({ {color={255,255,255,255}}, "STATS"})
  ititle:SetFont(titlefont)
  ititle:SetPos(0,45)
  ititle:CenterX()
  ititle.outline = true
  
  local backbox = loveframes.Create("image", frame)
  backbox:SetImage("gui/generic_box.png")
  backbox:SetScale(0.5, 0.5)
  backbox:SetPos(37, 285)
                
  local itext = loveframes.Create("text", frame)
  itext:SetFont(textfont)
  itext:SetPos(40,80)
  
  local iskills = loveframes.Create("text", frame)
  iskills:SetFont(textfont)
  iskills:SetPos(45,290)
  
  frame.child = {}
  frame.child["itext"] = itext
  frame.child["obj"] = obj
  frame.child["iskills"] = iskills
  
  control.updateStats()
end

function control.updateStats()
  local frame = control.stats_frame
  if not frame then return end
  
  local itext = frame.child["itext"]
  local obj = frame.child["obj"]
  local iskills = frame.child["iskills"]
  
  local name = obj.name
  if name == "you" then name = "Alice" end
  
  local function print_stat(S, name)
    local lvl_list = battle.lvl_list
    local lvl = S[3]+S[4]
    if lvl < 1 then lvl = 1 end
    if lvl > #lvl_list then lvl = #lvl_list end
    return "Lvl." .. S[3] .. " | " .. name .. ": " .. S.f .. " (" .. lvl_list[lvl]*100 .. "%)\n"
  end
  
  local function print_points(p)
    local sign = ""
    if p[3] > 0 then sign = "+" end
    return p[2] .. " (" .. sign .. p[3] .. ")\n"
  end
  
  local function print_skills(obj)
    return "You know nothing John Snow"
  end
  
  battle.calc_stats(obj)
  local text =  "Name: " .. name .. "\n\n" .. 
                "[HP] Health: " .. print_points(obj.hp) ..
                "[SP] Stamina: " .. print_points(obj.sp) ..
                "[MP] Magic: " .. print_points(obj.mp) ..
                "\n" ..
                print_stat(obj.stat.attack, "Attack") ..
                print_stat(obj.stat.defense, "Defense") ..
                print_stat(obj.stat.evasion, "Evasion") ..
                print_stat(obj.stat.endurance, "Endurance") ..
                "\n" .. "Skills:\n"
                
  itext:SetText({ {color={255,255,255,255}}, text})
  iskills:SetText({ {color={255,255,255,255}}, print_skills()})      
end

return control