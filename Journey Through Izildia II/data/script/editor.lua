local Editor = {}

function Editor:init()
  self.ctrlkeypressed = false
  Signal.register("nextscene-location", function() self:updateSlideWindow() end)
end

function Editor:enter()
  local scn = self.scene
  if scn.editmode then 
    scn.prev = loveframes.Create("imagebutton", frame)
    scn.prev:SetImage("gui/sprite/next_arrow.png")
    scn.prev:SetImageHover("gui/sprite/next_arrow_light.png")
    scn.prev:SetPos(780, 10)
    scn.prev:SetText("")
    scn.prev.rotation = math.pi
    
    scn.prev.OnClick = function(object, x, y)
      self:SetEditable(false)
      scn.index = scn.index - 1
      if scn.queue[scn.index] ~= nil then
        local id = scn.queue[scn.index]
        Effect.Timer:clear()
        scn:loadSlide()
        Signal.emit("nextscene-location", id)
      end
    end
  end
end

function Editor:update(dt)
  local scn = self.scene
  self.ctrlkeypressed = love.keyboard.isDown('lctrl')
  if scn.editmode then
    if love.mouse.isDown(1) and not scn.itext:GetEditable() and scn.itext:GetFocus() then
      scn.itext:SetPos(love.mouse.getPosition())
    end
    if love.mouse.isDown(1) and not scn.itext:GetEditable() and scn.ititle:GetFocus() then
      scn.ititle:SetPos(love.mouse.getPosition())
    end
    if self.ctrlkeypressed then
      if love.keyboard.isDown('q') then scn.itext:SetWidth(scn.itext:GetWidth()+1) end
      if love.keyboard.isDown('w') then scn.itext:SetWidth(scn.itext:GetWidth()-1) end
      if love.keyboard.isDown('a') then scn.itext:SetHeight(scn.itext:GetHeight()+1) end
      if love.keyboard.isDown('s') then scn.itext:SetHeight(scn.itext:GetHeight()-1) end
    end 
  end
end

function Editor:SetEditable(bool)
  local scn = self.scene
  if scn.editmode == false then return end
  if scn.itext:GetEditable() == bool then return end
  scn.itext:SetEditable(bool)
  scn.ititle:SetEditable(bool)
  if not bool then self:saveLang() end
  Effect.Timer:finish()
  if scn.itext:GetEditable() then
    scn.itext:SetText(scn.bbcode.realtext)
    scn.bbcode.clean(scn.itext)
  else
    scn.bbcode.make(scn.itext)
  end
end

function Editor:keyreleased(key)
  local scn = self.scene
  if scn.editmode and self.ctrlkeypressed then
    if key == "e" then
      self:SetEditable(not scn.itext:GetEditable())
    end
    if key == "r" then
      self:openSlideWindow()
    end
  end
end

function Editor:saveLang()
  local scn = self.scene
  local name = scn.queue[scn.index]
  quest.data[name].title.text = scn.ititle:GetText()
  quest.data[name].title.position = {scn.ititle:GetX(), scn.ititle:GetY()}
  quest.data[name].text.text = scn.itext:GetText()
  quest.data[name].text.position = {scn.itext:GetX(), scn.itext:GetY()}
  quest.data[name].text.size = {scn.itext:GetWidth(), scn.itext:GetHeight()}
  
  Lady.save_all("/lang/" .. language .. "/quest.txt", quest.data)
end

function Editor:openSlideWindow()
  local scn = self.scene
  if self.frame then return end
  
  local loveframes_state = loveframes.GetState()
  loveframes.SetState("none")
  
  self.frame = loveframes.Create("frame")
  self.frame:SetSize(200, 200)
  self.frame:SetName("Slide Options")
  self.frame:Center()
  self.frame.OnClose = function(object)
    object:Remove()
    self.frame = nil
    loveframes.SetState(loveframes_state)
  end
  
  function create_textbox(text, width)
    local input = loveframes.Create("textinput", self.frame)
    input:SetWidth(width)
    input:SetText(text)
    input.colormask = {{0,0,0,255}}
    return input
  end
  
  local lname = loveframes.Create("text", self.frame)
  lname:SetText("ID: ")
  
  local iname = create_textbox(scn.queue[scn.index], 200-25)
  
  local button = loveframes.Create("button", self.frame)
  button:SetText("Apply")
  
  local lbkgd = loveframes.Create("text", self.frame)
  lbkgd:SetText("Bkgd: ")
  
  local data = quest.data[scn.queue[scn.index]]
  local ibkgd = create_textbox(data.background, 200-43)
  
  local laux = loveframes.Create("text", self.frame)
  laux:SetText("Image: ")
  
  local aux_path = ""
  if data.aux and data.aux.path then aux_path = data.aux.path end
  local iaux = create_textbox(aux_path, 193-45)
  
  local auxx, auxy = 0, 0
  if data.aux and data.aux.x then auxx = data.aux.x end
  if data.aux and data.aux.y then auxy = data.aux.y end
  
  local iauxx = create_textbox(auxx, 50)
  local iauxy = create_textbox(auxy, 50)
  
  local dupli = loveframes.Create("checkbox", self.frame)
  dupli:SetText("Duplicate")
  
  lname:SetPos(5, 30)
  iname:SetPos(22,25)
  button:SetPos(5,170)
  button:CenterX()
  lbkgd:SetPos(5,60)
  ibkgd:SetPos(40,55)
  laux:SetPos(5,90)
  iaux:SetPos(48,85)
  iauxx:SetPos(48, 115)
  iauxy:SetPos(100, 115)
  dupli:SetPos(5, 140)
  
  self.frame.iname = iname
  self.frame.lname = lname
  self.frame.ibkgd = ibkgd
  self.frame.lbkgd = lbkgd
  
  button.OnClick = function(object)
    local name = scn.queue[scn.index]
    if iname:GetText() ~= name then
      local new_name = iname:GetText()
      quest.data[new_name] = quest.data[name]
      quest.data[name] = nil
      
      if dupli:GetChecked() == true then
        quest.data[name] = loveframes.util.DeepCopy(quest.data[new_name])
      end
      
      name = new_name
      scn.queue[scn.index] = name
    end
    
    quest.data[name].background = ibkgd:GetText()
    
    if not quest.data[name].aux then
      quest.data[name].aux = {
          path = nil,
          x = 0, y = 0
      }
    end
    quest.data[name].aux.path = iaux:GetText()
    quest.data[name].aux.x = iauxx:GetText()
    quest.data[name].aux.y = iauxy:GetText()
    
    self:SetEditable(false)
    self.frame:OnClose()
    battle.finish()
    scn:loadSlide()
  end
  
end

function Editor:updateSlideWindow()
  local scn = self.scene
  if self.frame == nil then return end
  self.frame.iname:SetText(scn.queue[scn.index])
  
end

return Editor