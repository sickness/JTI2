local q = {}
q.name = "Main"
q.use = true

function q.enterLocation(loc)
  if not q.use then return end
  
  quest.times[loc] = quest.times[loc] + 1
  if quest.times[loc] == 1 and (not key_in(loc, {"river", "assembly_hall"})) then
    quest.pushFront("fta_" .. loc)
    return
  end
 
  
  if not quest.isEmpty() then return end
  if loc == "river" then quest.pushBack("default_river_0") end
  if loc == "assembly_hall" then quest.pushBack("default_assembly_0") end
  if loc == "huts" then quest.pushBack("default_huts_0") end
  if loc == "forest_edge" then quest.pushBack("default_forestedge_0") end
  if loc == "training_camp" then quest.pushBack("default_trainingcamp_0") end
end

function q.exitLocation(loc)
  q.use = true
end

quest.register(q)
return q