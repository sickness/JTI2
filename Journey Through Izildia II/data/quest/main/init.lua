local q = {}
q.name = "Main"
q.chapter = 1
q.state = 4

local ch = {
  require("quest.main.intro")
}

function q.load()
  ch[q.chapter].load()
end

function q.battleFinish(id)
  ch[q.chapter].battleFinish(id)
end

function q.enterLocation(loc)
  ch[q.chapter].enterLocation(loc)
end

function q.exitLocation(loc)
  ch[q.chapter].exitLocation(loc)
end

function q.nextScene(id)
  ch[q.chapter].nextScene(id)
end


quest.register(q)

for _, chapter in ipairs(ch) do
  chapter.init(q)
end

return q